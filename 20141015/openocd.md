# OpenOCD

OpenOCD dient der Verbindung vom PC zum integrierten Emulator/Debugger des verwendeten Eval-Boards.

## Installation

### Windows

Entpacken Sie das [Zip-Archiv](http://www.freddiechopin.info/en/download/category/4-openocd) in einen Ordner Ihrer Wahl z.B **"c:\Programme\OpenOCD-0.8.0"**
Fügen Sie den Pfad **"c:\Programme\OpenOCD-0.8.0\bin"** zu Ihrem [Systempfad hinzu](http://www.java.com/de/download/help/path.xml).
Unter Windows heißt das Programm **openocd-0.8.0** und nicht **openocd**

### Mac OSX

Unter Mac OSX kann OpenOCD mittels [HomeBrew](http://brew.sh/) installiert werden.
**Achtung es wird Version 0.8.x benötigt**

Damit HomeBrew installiert werden kann wird Ruby benötigt.


### Test der Installation

Nachdem Sie OpenOCD zu Ihrem Pfad hinzugefügt haben öffnen Sie ein Terminal/Komando-Shell und geben das Kommando openocd ein. Sie sollten eine Meldung die in etwa wie folgt lautet sehen:

    Open On-Chip Debugger 0.8.0-dev-00379-g17fddb4 (2014-03-09-11:40)
    Licensed under GNU GPL v2
    For bug reports, read
    	http://openocd.sourceforge.net/doc/doxygen/bugs.html
    Runtime Error: embedded:startup.tcl:47: Can't find openocd.cfg
    in procedure 'script'
    at file "embedded:startup.tcl", line 47
    Error: Debug Adapter has to be specified, see "interface" command
    in procedure 'init'

Unter Windows heißt das Programm **openocd-0.8.0** und nicht **openocd** die Ausgabe sieht wie folgt aus:

    C:\Users\deegech>openocd-0.8.0
    Open On-Chip Debugger 0.8.0 (2014-04-28-08:39)
    Licensed under GNU GPL v2
    For bug reports, read
            http://openocd.sourceforge.net/doc/doxygen/bugs.html
    Runtime Error: embedded:startup.tcl:47: Can't find openocd.cfg
    in procedure 'script'
    at file "embedded:startup.tcl", line 47
    Error: Debug Adapter has to be specified, see "interface" command
    in procedure 'init'


Erhalten Sie eine Fehlermeldung das das Programm nicht gefunden werden kann hat das Hinzuügen zum Systempfad nicht geklappt.