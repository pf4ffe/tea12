# Doxygen

Bei Doxygen handelt es sich um eine Software zur Dokumentation Ihres Projektes/Quellcodes.

## Installation

### Windows

Unter Windows können Sie Doxygen mittels des [Setup-Programms](http://www.stack.nl/~dimitri/doxygen/download.html) installieren.

### Linux

Unter Linux kann Doxygen mittels der Paketverwaltung Ihrere Distribution installiert werden.

### Mac OS X

Unter Mac OS können Sie Doxygen entweder mittels [HomeBrew](http://brew.sh/) oder über das Disk-Image auf der
[Doxygen Webseite](http://www.stack.nl/~dimitri/doxygen/download.html) installieren.
