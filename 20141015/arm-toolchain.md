# GNU Tools for ARM Embedded Processors

Diese Tools dienen der Übersetzung Ihrer C-Dateien in das Maschinenprogramm für den uC.

## Installation

Die Installations Pakete für Ihr Betriebsystem finden Sie auf der [Projektseite](https://launchpad.net/gcc-arm-embedded/4.8/4.8-2013-q4-major). **Achtung wir verwenden die Version 4.8**

### Windows

Installieren Sie die die Toolchain mittels der bereitgestllten Installations-rutine ( gcc-arm-none-eabi-4_8-2013q4-20131204-win32.exe).


### Mac OS X

Entpacken Sie den Tar-Ball in ein Verzeichnis Ihrer Wahl z.B: */Users/XXXXX/install/gcc-arm-none-eabi-4_8-2013q4* ersetzen Sie **XXXXX** durch Ihren Benutzernamen.